<?php
namespace Travers\Frontpage;

\add_action( 'rest_api_init', function() {
  \register_rest_route( 'travers/v1', '/frontpage/',
    [
      'methods'   => 'GET',
      'callback'  => __NAMESPACE__.'\rest_results'
    ]
  );
} );

\add_action( 'rest_api_init', function() {
  \register_rest_route( 'travers/v1', '/authentication/',
    [
      'methods'   => 'POST',
      'callback'  => __NAMESPACE__.'\check_auth'
    ]
  );
} );

function check_auth( $request ) {

  $data = [
    'yes'      => 'yes',
  ];

  return new \WP_REST_Response( $data, 200 );
}


function rest_results( $request ) {
  $fp_id  = (int) \get_option( 'page_on_front' );

  $post = ( $fp_id > 0 ) ? \get_post( $fp_id ) : null;

  if( ! is_a( $post, '\WP_Post' ) )
    return new \WP_Error( 'error',
      \esc_html__( 'No Static Frontpage', 'travers' ), [ 'status' => 404 ] );

  // Response setup
  $galerie = \get_field( 'galerie', $post->ID );
  $formatted_slides = array();
  foreach ($galerie as $slide) {
    $newslide = (array)\get_post( $slide );
    $newslide['srcset'] = \wp_get_attachment_image_srcset( $slide );
    $newslide['source']       = \wp_get_attachment_image_src( $slide, 'large' )[0];
    $newslide['alt']          = \get_post_meta( $slide, '_wp_attachment_image_alt', TRUE );
    $formatted_slides[] = $newslide;
  }
  $data = [
    'id'      => $post->ID,
    'galerie' => $formatted_slides,
  ];

  return new \WP_REST_Response( $data, 200 );
}
