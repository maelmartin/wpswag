<?php

add_action( 'graphql_register_types', function() {

  register_graphql_field( 'Page', 'homepage', [
     'type' => 'Boolean',
     'description' => __( 'Est-ce que cette page est la homepage ?' ),
     'resolve' => function( $page ) {
       $hp_id  = (int) \get_option( 'page_on_front' );
       return $page->ID == $hp_id;
     }
  ] );

  register_graphql_field('RootQueryToPageConnectionWhereArgs', 'homepage', [
        'type' => 'Boolean',
        'description' => __('Est-ce que cette page est la homepage ?' ),
    ]);
} );

add_filter('graphql_post_object_connection_query_args', function ($query_args, $source, $args, $context, $info) {

    $homePage = $args['where']['homepage'];

    if ( isset( $homePage ) && $homePage == true ) {
      $hp_id  = (int) \get_option( 'page_on_front' );
      $query_args['page_id'] = $hp_id;
    }

    return $query_args;
}, 10, 5);
