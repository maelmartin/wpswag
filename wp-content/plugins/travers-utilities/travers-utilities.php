<?php
/**
 * Plugin Name: Travers Média - Utilities
 * Plugin URI: https://www.travers-media.com
 * Description: Fonctions et endpoints personnalisés
 * Version: 1.0
 * Author: Travers Média
 * Author URI: https://www.travers-media.com
 */

require_once( 'utilities/rest.frontpage.php' );
require_once( 'utilities/graphql.frontpage.php' );
