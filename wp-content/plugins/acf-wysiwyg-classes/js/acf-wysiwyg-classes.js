(function($) {
	acf.add_filter('wysiwyg_tinymce_settings', function(mceInit, id, $field) {
		// var mceInitElements = $('#' + mceInit.elements);
		// var mceInitElements = $('#' + id + '-wrap');
		// console.log(mceInit);
		// var acfEditorField = mceInitElements.closest('.acf-field-wysiwyg');
		var fieldKey = $field.data('key');
		var fieldName = $field.data('name');
	  var flexContentName = $field.parents('[data-type="flexible_content"]').first().data('name');
		var layoutName = $field.parents('[data-layout]').first().data('layout');
		mceInit.body_class += " acf-key-" + fieldKey;
		mceInit.body_class += " acf-name-" + fieldName;
		if (flexContentName) {
			mceInit.body_class += " acf-flex-" + flexContentName;
		}
		if (layoutName) {
			mceInit.body_class += " acf-layout-" + layoutName;
		}
		return mceInit;
	});
})(jQuery);
