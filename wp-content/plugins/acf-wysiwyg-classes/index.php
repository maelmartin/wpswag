<?php
/*
Plugin Name: ACF WYSIWYG classes
Version: 1.0
Author: David Félix-Faure
Author URI: http://www.felixfaure.fr/
Description: Add classes to the body of WYSIWYG editor. Good for styling ;)
*/

function acf_wysiwyg_classes_plugin() {
	wp_register_script( 'acf-wysiwyg-classes', plugins_url( 'js/acf-wysiwyg-classes.js', __FILE__ ), array('acf-input'));
  wp_enqueue_script( 'acf-wysiwyg-classes' );
	// echo '<script src="' . plugins_url( 'js/acf-wysiwyg-classes.js', __FILE__ ) . '"></script>';
}
add_action('acf/input/admin_enqueue_scripts', 'acf_wysiwyg_classes_plugin');
?>
