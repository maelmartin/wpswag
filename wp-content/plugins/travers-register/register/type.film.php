<?php
//================================================================================================
//Film : Ajout du custom post type
//================================================================================================
function my_custom_post_type_film() {
	$labels = array(
		'name'               => _x( 'Films', 'post type general name', 'grandr' ),
		'singular_name'      => _x( 'Film', 'post type singular name', 'grandr' ),
		'add_new_item'       => __( 'Ajouter un film', 'grandr' ),
		'new_item'           => __( 'Nouveau film', 'grandr' ),
		'edit_item'          => __( 'Modifier', 'grandr' ),
		'view_item'          => __( 'Voir le film', 'grandr' ),
		'all_items'          => __( 'Tous les films', 'grandr' ),
		'search_items'       => __( 'Rechercher un film', 'grandr' ),
		'not_found'          => __( 'Aucun film trouvé', 'grandr' ),
		'not_found_in_trash' => __( 'Aucun film dans la corbeille', 'grandr' ),
		// 'parent_item_colon'  => '', //Seulement si hierarchical
		'archives'					 => __( 'Archive des films', 'grandr' ),
	);
	$args = array(
		'labels' => $labels,
		'public' => true, //= false
		// 'exclude_from_search' => true, //= !public
		// 'publicly_queryable' => false, //= public
		// 'show_ui' => false, //= public
		'show_in_nav_menus' => false, //= public
    'show_in_rest' => true,
    'rest_base' => 'films',
		// 'show_in_menu' => false, //= show_ui
		// 'show_in_admin_bar' => false, //= show_in_menu
		// 'menu_position' => 10, //= 5
		'menu_icon' => 'dashicons-video-alt2',
		'map_meta_cap'  => true,
		'capability_type' => array('film', 'films'),
		// 'hierarchical' => true, //= false
		'supports' => array('title','editor','revisions'), //= title and editor
		// 'taxonomies' => array('category'),
		'has_archive'	=> true, //= false
		'rewrite' => true,
	);
	register_post_type( 'film', $args );
}
add_action( 'init', 'my_custom_post_type_film' );


//================================================================================================
//Film : Permaliens
//================================================================================================
//Permalink
// add_filter( 'post_type_link', 'custom_post_type_link_film', 10, 3 );
function custom_post_type_link_film( $permalink, $post, $leavename ) {
	if ( $post->post_type == 'film' ) :
		$replace = __( 'film', 'grandr' );
		$saison = get_field( 'saison', $post->ID );
		if ( $saison && !is_object( $saison ) )
			$saison = new Saison( $saison );
		if ( !empty( $saison->slug ) )
			$replace = __( 'saison', 'grandr' ).'/'.$saison->slug;
		$permalink = str_replace( '%saison%', $replace, $permalink );
	endif;
	return $permalink;
}

//Lien d'archive
// add_filter( 'post_type_archive_link', 'custom_archive_link_film', 10, 2);
function custom_archive_link_film( $link, $post_type ) {
	if ( $post_type && $post_type == 'film' ) :
		$template_link = link_by_template( 'template-archives-films' );
		$link = $template_link ? $template_link : esc_url( home_url( '/' ) ).__('cinema','grandr').'/';
	endif;

	return $link;
}


//================================================================================================
//Film : modification du template archive
//================================================================================================
// add_filter( 'template_include', 'custom_archive_film_template', 99 );
function custom_archive_film_template( $template ) {
	if ( is_post_type_archive('film') || is_tax( array('taxo_name') ) ) {
		$new_template = locate_template( array( 'template-film.php' ) );
		if ( '' != $new_template )
			return $new_template ;
	}
	return $template;
}


//================================================================================================
//Film : variables autorisées
//================================================================================================
// add_filter( 'query_vars', 'add_query_vars_film' );
function add_query_vars_film( $vars ){
  $vars[] = "custom_var";
  return $vars;
}


//================================================================================================
//Film : Modification de la requête
//================================================================================================
// add_action('pre_get_posts','custom_query_film');
function custom_query_film($query) {
	if ( is_admin() || !$query->is_main_query() ) return;

	if ( is_post_type_archive( 'film' ) ) :
		// $query->set( 'posts_per_page', -1 );
		// $custom_var = get_query_var('custom_var',false);
		// $query->set( 'meta_query', array(
		// 	'relation' => 'AND',
		// 	array (
		// 		'key' => 'dates_%_date',
		// 		// 'value' => $dates,
		// 		// 'compare' => 'IN',
		// 		// 'type' => 'NUMERIC'
		// 		'value' => array( $dates[0].' 00:00:00', $dates[count($dates) - 1].' 00:00:00' ),
		// 		'type' => 'DATETIME',
		// 		'compare' => 'BETWEEN'
		// 	)
		// ) );
    return;
  endif;
}

//Pouvoir faire une requete like sur les meta key
// add_filter( 'posts_where' , 'allow_wildcards_film');
function allow_wildcards_film($where) {
	$where = str_replace("meta_key = 'dates_%_date", "meta_key LIKE 'dates_%_date", $where);
  return $where;
}


//================================================================================================
//Film : titre auto (Nom prénom)
//================================================================================================
function title_auto_film($post_id) {
	if ( empty($_POST['post_type']) || 'film' != $_POST['post_type'] ) return;
	remove_action( 'save_post', 'title_auto_film' );
	$names = array();
	$nom = get_field('nom',$post_id);
	$prenom = get_field('prenom',$post_id);
	if ( !empty($nom) ) $names[] = ucname($nom);
	if ( !empty($prenom) ) $names[] = ucname($prenom);
	if ( !empty($names) ) :
		$title = implode(' ',$names);
		wp_update_post( array( 'ID' => $post_id, 'post_title' => $title, 'post_name' => $title ) );
	endif;
	add_action( 'save_post', 'title_auto_film' );
}
// add_action( 'save_post', 'title_auto_film' );


//================================================================================================
//Film : Messages du custom post type
//================================================================================================
function film_updated_messages( $messages ) {
	$post             = get_post();
	$post_type        = 'film';
	if ( $post->post_type != $post_type ) return $messages;
	$post_type_object = get_post_type_object( $post_type );

	$messages[$post_type] = array(
		0  => '', // Unused. Messages start at index 1.
		1  => __( 'Film mis à jour.', 'grandr' ),
		2  => __( 'Champ personnalisé mis à jour.', 'grandr' ),
		3  => __( 'Champ personnalisé supprimé.', 'grandr' ),
		4  => __( 'Film mis à jour.', 'grandr' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Film restauré à la révision du %s', 'grandr' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6  => __( 'Film publié.', 'grandr' ),
		7  => __( 'Film sauvegardé.', 'grandr' ),
		8  => __( 'Film envoyé.', 'grandr' ),
		9  => sprintf(
			__( 'Film prévu pour le : <strong>%1$s</strong>.', 'grandr' ),
			// translators: Publish box date format, see http://php.net/date
			date_i18n( __( 'j M Y @ G:i', 'grandr' ), strtotime( $post->post_date ) )
		),
		10 => __( 'Brouillon du film mis à jour.', 'grandr' )
	);

	if ( $post_type_object->publicly_queryable ) {
		$permalink = get_permalink( $post->ID );

		$view_link = sprintf( ' <a href="%s">%s</a>', esc_url( $permalink ), __( 'Voir le film', 'grandr' ) );
		$messages[ $post_type ][1] .= $view_link;
		$messages[ $post_type ][6] .= $view_link;
		$messages[ $post_type ][9] .= $view_link;

		$preview_permalink = add_query_arg( 'preview', 'true', $permalink );
		$preview_link = sprintf( ' <a target="_blank" href="%s">%s</a>', esc_url( $preview_permalink ), __( 'Prévisualiser le film', 'grandr' ) );
		$messages[ $post_type ][8]  .= $preview_link;
		$messages[ $post_type ][10] .= $preview_link;
	}

	return $messages;
}
add_filter( 'post_updated_messages', 'film_updated_messages' );


//================================================================================================
//Film : modification des colonnes
//================================================================================================
// add_filter('manage_film_posts_columns' , 'set_film_columns', 11);
function set_film_columns($columns) {
	$new_columns = array();
	$new_columns['cb'] = $columns['cb'];
	$new_columns['image'] = __('image');
	$new_columns['title'] = $columns['title'];
	// $new_columns['categories'] = $columns['categories'];
	// $new_columns['taxonomy-auteur'] = $columns['taxonomy-auteur'];
	if ( !empty($columns['icl_translations']) ) $new_columns['icl_translations'] = $columns['icl_translations'];
	$new_columns['date'] = $columns['date'];
	return $new_columns;
}

// add_action( 'manage_film_posts_custom_column' , 'custom_film_column', 10, 2 );
function custom_film_column( $column, $post_id ) {
  switch ( $column ) {
  	case 'image' :
			$postImg = get_field('film_cover',$post_id);
			if( !empty($postImg) ) echo '<img src="'.$postImg['sizes']['thumbnail'].'">';
      break;
  }
}

// add_filter( 'manage_edit-film_sortable_columns', 'custom_film_sortable_columns', 10, 1 );
function custom_film_sortable_columns( $columns ) {
  $columns['dates'] = 'dates';
  return $columns;
}

// add_action( 'pre_get_posts', 'custom_film_column_orderby' );
function custom_film_column_orderby( $query ) {
  if( ! is_admin() )
    return;
  $orderby = $query->get( 'orderby');
  if( 'date-disque' == $orderby ) {
    $query->set('meta_key','date_sortie');
    $query->set('orderby','meta_value_num');
  }
}
?>
