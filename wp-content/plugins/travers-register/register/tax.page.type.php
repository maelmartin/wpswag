<?php
//================================================================================================
//Type de page : Création de la taxonomie

//-------------------------------------
//Intitulés pour rechercher/remplacer :
//-------------------------------------
//types_de_page (cine-genres) (capability_type plural)
//type_de_page (cine-genre) (function, post type name, capability_type singular)
//types de page (genres)
//type de page (genre)
//Types de page (Genres)
//Type de page (Genre)

//le  ("le " ou "la " ou "l’")
//nouveau ("nouveau" ou "nouvelle" ou "nouvel")
//Tous ("Tous" ou "Toutes")
//du  ("du " ou "de la " ou "de l’")
//de  ("de " ou "d’")
// (pour ajouter un e aux mots, exemple : une, aucune, trouvée)
//================================================================================================
function register_custom_tax_type_de_page() {
	$labels = array(
		'name'              => _x( 'Types de page', 'taxonomy general name' ),
		'singular_name'     => _x( 'Type de page', 'taxonomy singular name' ),
		'search_items'      => __( 'Recherche un type de page' ),
		'all_items'         => __( 'Tous les types de page' ),
		'parent_item'       => __( 'Parent du type de page' ), //for hierarchical
		'parent_item_colon' => __( 'Parent du type de page :' ), //for hierarchical
		'edit_item'         => __( 'Editer le type de page' ),
		'view_item'					=> __( 'Voir le type de page' ),
		'update_item'       => __( 'Mettre à jour le type de page' ),
		'add_new_item'      => __( 'Ajouter un nouveau type de page' ),
		'new_item_name'     => __( 'Nouveau nom de type de page' ),
		// 'menu_name'         => __( 'Types de page' ), //=name
		'popular_items'			=> __( 'Types de page populaires' ), //for non-hierarchical
		'separate_items_with_commas' => __( 'Séparer les types de page avec des virgules' ), //for non-hierarchical
		'add_or_remove_items' => __( 'Ajouter ou supprimer des types de page' ), //for non-hierarchical
		'choose_from_most_used' => __( 'Choisir parmis les types de page les plus utilisés' ), //for non-hierarchical
		'not_found'					=> __( 'Pas de type de page trouvé' ),
	);
	$args = array(
		'labels'						=> $labels,
		// 'public'						=> false, //= true
		// 'publicly_queryable'=> false,
		// 'show_ui'		      	=> false, //= public
		// 'show_in_menu' 			=> false, //= show_ui
		// 'show_in_nav_menus' => false, //= public
		'show_tagcloud'			=> false, //= show_ui
		// 'show_in_quick_edit'=> false, //= show_ui
		'show_admin_column' => true, //= show_ui
		// 'hierarchical'      => true, //= false
		'rewrite'           => true,
		'capabilities'      => array(
			'manage_terms'    => 'manage_types_de_page',
			'edit_terms'      => 'edit_types_de_page',
			'delete_terms'    => 'delete_types_de_page',
			'assign_terms'    => 'assign_types_de_page',
		),
		// 'sort'							=> true, //=false
	);
	register_taxonomy( 'type_de_page', array('page'), $args );

	// add_permastruct( 'type_de_page', '/type_de_page/%type_de_page%', false );
}
add_action( 'init', 'register_custom_tax_type_de_page', 0 );


//================================================================================================
//Type de page : archive redirection ou modification du lien
//================================================================================================
// Redirection
// add_action( 'template_redirect', 'redirect_taxo_type_de_page_to' );
function redirect_taxo_type_de_page_to() {
  if( is_tax( 'type_de_page' ) ) :
		$url = esc_url( home_url( '/' ) );
		$tax = get_queried_object();
		if ( !empty($tax->taxonomy) && $tax->taxonomy == "type_de_page" ) :
		  $tax = new Taxo($tax);
			// $url = '...';
		endif;
    wp_redirect( $url, 301 );
    exit();
	endif;
}

// Modification du lien
// add_filter('term_link', 'custom_taxo_type_de_page_link', 10, 3);
function custom_taxo_type_de_page_link( $url, $term, $taxonomy ) {
	if ( $taxonomy == 'type_de_page' ) :
		$template_link = link_by_template("template-contacts");
		$url = $template_link ? $template_link.'#lieu-'.$term->slug : '';
	endif;

	return $url;
}
?>
