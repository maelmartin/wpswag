<?php
/**
 * Plugin Name: Travers Média - Types de contenu
 * Plugin URI: https://www.travers-media.com
 * Description: Enregistrement des types de contenu personnalisés pour le projet
 * Version: 1.0
 * Author: Travers Média
 * Author URI: https://www.travers-media.com
 */
 require_once( 'register/type.film.php' );
require_once( 'register/tax.page.type.php' );
