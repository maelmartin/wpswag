<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wptest' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost:3307' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'J0Yu l7o5G,4Z0uB8.t|oX1X:J&!<&|zP(U J~[U(sl>}=|._}cM9n=od[1yb7)P' );
define( 'SECURE_AUTH_KEY',  'Y];hH*IK:j!;Z!>vE{Pg3.coVy#!zu:BnGU^+:TuutZCG9IGEF%~c1ym1_ds@t%u' );
define( 'LOGGED_IN_KEY',    '>tfoHZHZY*^B?EtXR0Fn=[j,}`@XceVS]2vf} %`p=:qp&+Mc>cR2,aJ]$$$MhAR' );
define( 'NONCE_KEY',        'T0>W%xG#ne{<@EsG6;9e#Z$?}JaA2X(Ei(s8J 8|UEO!fqJ}Ew0F#+Q{VKLqCZ=$' );
define( 'AUTH_SALT',        'fw}ej@QN0fh<J O@Y3rD22LEqK8xDI.4Vs4(t*JLm}C+SX7dhGr<Y~HZp4CGO>T!' );
define( 'SECURE_AUTH_SALT', 'fb,l_Vju*ot<yeRt%b6.Y?|)9;AbYF%QM^q(80aF}pa9}vQ;Zr^iADV%7^FR)tpV' );
define( 'LOGGED_IN_SALT',   'HN=lH:[g}ua:?;w.:>N0[*@6*O#DCbXjVBF%*am01MuU.lu]<;H%(jH3!y;8lf5t' );
define( 'NONCE_SALT',       'Gm[nDu|TzOARlN,#|,$c.yj#rgKs0eYu1WJ0$6%Pb74SCFQ}IZ,g&HVz&0!fDq`w' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
